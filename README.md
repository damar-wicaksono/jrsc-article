# JRSC Article on Bayesian Calibration of Reflood Simulation in TRACE

Journal of Royal Statistical Society, Series C: Applied Statistics

Tentative Title: Bayesian Calibration of Time-Dependent Computer Model with 
Application to Nuclear Engineering Thermal-Hydraulics Model

Authors: Damar Wicaksono, Omar Zerkak, and Andreas Pautz
